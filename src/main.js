// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
/* eslint-disable */
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'swiper/dist/css/swiper.min.css'
import 'swiper/dist/js/swiper.min'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import global_const from './components/global-const/global'
//引入仓库
import store from './store'

import BaiduMap from 'vue-baidu-map'



Vue.prototype.global_const = global_const;
Vue.use(VueAxios, Axios);
Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.use(BaiduMap, {
    ak: '5qh64YXzL8nWZyeQgIewdHEt2WFLKt3l'
});
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>',
    render: h => h(App)
})