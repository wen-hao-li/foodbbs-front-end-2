import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

//state 仓库
const state = {};
//mutation：修改state
const mutations = {};
//action 处理action，处理业务逻辑，也可以处理异步
const actions = {};
//getters:计算属性，简化仓库数据
const getters = {};

//对外暴露
export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
});