/* eslint-disable */

import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

import Home from '@/pages/Home'
import Login from '@/pages/Login'
import Register from '@/pages/Register'
import CenterHealth from '@/pages/Center-health'
import Community from '@/pages/Community'
import FoodCourt from '@/pages/Food-court'
import EmailLogin from '../pages/login/email-login'
import PhoneLogin from '../pages/login/phone-login'
import FoodType from '@/pages/Food-type'
import Peoson from '../pages/center-setting'
import Create from '../pages/center-create'
import CreateHealth from "../components/create/health"
import CreateFood from "../components/create/food"
import Manage from "../pages/center-manage"
import ManageHealth from "../components/manage/health-manage"
import ManageFood from "../components/manage/food-manage"
import ArticlePage from '@/pages/article-page'
import Search from '@/pages/search'
import sFood from '@/components/search/food'
import sHealth from '@/components/search/health'

export default new Router({
    routes: [{
            path: '/',
            redirect: '/home'

        },
        {
            path: '/article/page/:item',
            name: "articlePage",
            component: ArticlePage
        },

        {
            path: '/home',

            component: Home
        }, {
            path: '/login',
            component: Login,
            children: [{
                    path: '/login/email',
                    component: EmailLogin
                },
                {
                    path: '/login/phone',
                    component: PhoneLogin
                }
            ],
            redirect: '/login/email'
        },
        {
            path: '/register',
            component: Register
        },
        {
            path: '/center/health:type',
            component: CenterHealth,
            name: "CenterHealth"
        },
        {
            path: '/food/type/:style/:lib',
            component: FoodType,
            name: "FoodType",
        },
        {
            path: '/food/Court/:info?',
            component: FoodCourt,
            name: "FoodCourt"
        },
        {
            path: '/center/setting',
            component: Peoson
        },
        {
            path: '/center/create',
            component: Create,
            children: [{
                    path: '/center/create/health',
                    component: CreateHealth
                },
                {
                    path: '/center/create/food',
                    component: CreateFood
                }
            ],
            redirect: '/center/create/health'
        },
        {
            path: '/article/manage',
            component: Manage,
            children: [{
                    path: "/article/manage/health",
                    component: ManageHealth
                },
                {
                    path: "/article/manage/food",
                    component: ManageFood
                }
            ],
            redirect: "/article/manage/health"
        },
        {
            path: '/community/center',
            component: Community
        },
        {
            path: '/search/bar/:keyword?',
            component: Search,
            name: 'Search',
            children: [{
                path: '/search/bar/food/:keyword?',
                component: sFood,
                name: 'sFood',
            }, {
                path: '/search/bar/health/:keyword?',
                component: sHealth,
                name: 'sHealth',
            }],
            redirect: '/search/bar/food/:keyword?'

        }
    ],
    scrollBehavior(to, from, savedPosition) {

        if (savedPosition) {

            return savedPosition // 按下 后退/前进 按钮时，类似浏览器的原生表现

        } else {

            return { x: 0, y: 0 } // 让页面滚动到顶部
        }
    }
})